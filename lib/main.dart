import 'package:custom_bottom_bar/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(
      MaterialApp(
        home: HomeScreen(index: 0,),
      ));
}

class HomeScreen extends StatefulWidget {

  final int index;

  HomeScreen({this.index});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  int _currentIndex;

  @override
  void initState() {
    // TODO: implement initState
    _currentIndex = widget.index;
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.transparent,
        ),
      ),
      bottomNavigationBar: bottomNav(),
    );
  }

  Widget bottomNav(){

    return Padding(
      padding: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal*4.5,0,SizeConfig.blockSizeHorizontal*4.5,100),
      child: Container(
        height: SizeConfig.blockSizeHorizontal*18,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.grey[900],
          borderRadius: BorderRadius.circular(30),
        ),

        child: Stack(

          children: [

            Positioned(
              top: 0,
              bottom: 0,
              left: SizeConfig.blockSizeHorizontal*3,
              right: SizeConfig.blockSizeHorizontal*3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  BottomNavButton(
                    onPressed: (val){
                      setState(() {
                        _currentIndex = val;
                      });
                    },
                    icon: CupertinoIcons.home,
                    currentIndex: _currentIndex,
                    index: 0,
                  ),

                  BottomNavButton(
                    onPressed: (val){
                      setState(() {
                        _currentIndex = val;
                      });
                    },
                    icon: CupertinoIcons.bookmark,
                    currentIndex: _currentIndex,
                    index: 1,
                  ),

                  BottomNavButton(
                    onPressed: (val){
                      setState(() {
                        _currentIndex = val;
                      });
                    },
                    icon: CupertinoIcons.add_circled,
                    currentIndex: _currentIndex,
                    index: 2,
                  ),

                  BottomNavButton(
                    onPressed: (val){
                      setState(() {
                        _currentIndex = val;
                      });
                    },
                    icon:CupertinoIcons.settings,
                    currentIndex: _currentIndex,
                    index: 3,
                  ),

                  BottomNavButton(
                    onPressed: (val){
                      setState(() {
                        _currentIndex = val;
                      });
                    },
                    icon: CupertinoIcons.heart,
                    currentIndex: _currentIndex,
                    index: 4,
                  ),
                ],
              ),
            ),

            AnimatedPositioned(
              duration: Duration(milliseconds: 300),
              curve: Curves.decelerate,
              top: 0,
              left:
              (_currentIndex == 0 ) ? SizeConfig.blockSizeHorizontal*5.5 :
              (_currentIndex == 1 ) ? SizeConfig.blockSizeHorizontal*22.5 :
              (_currentIndex == 2 ) ? SizeConfig.blockSizeHorizontal*39.5 :
              (_currentIndex == 3 ) ? SizeConfig.blockSizeHorizontal*56.5  :
              (_currentIndex == 4 ) ? SizeConfig.blockSizeHorizontal*73.5 : 0,

              child: Column(
                children: [
                  Container(
                    height: SizeConfig.blockSizeHorizontal*1.0,
                    width: SizeConfig.blockSizeHorizontal*12,
                    decoration: BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  ClipPath(
                    clipper: MyCustomClipper(),
                    child: Container(
                      height: SizeConfig.blockSizeHorizontal*15,
                      width: SizeConfig.blockSizeHorizontal*12,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.yellow.withOpacity(0.8),
                                Colors.yellow.withOpacity(0.5),
                                Colors.transparent
                              ]
                          )
                      ),
                    ),
                  ),
                ],
              )
            )
          ],
        )
      ),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(SizeConfig.blockSizeHorizontal*3, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo( size.width - SizeConfig.blockSizeHorizontal*3 , 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class BottomNavButton extends StatelessWidget {

  final Function(int) onPressed;
  final IconData icon;
  final int index;
  final int currentIndex;

  BottomNavButton({this.icon,this.onPressed,this.index,this.currentIndex, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return InkWell(
      onTap: (){
        onPressed(index);
      },
      child: Container(
        height: SizeConfig.blockSizeHorizontal*13,
        width: SizeConfig.blockSizeHorizontal*17,
        decoration: BoxDecoration(
          color: Colors.transparent,
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [


            (currentIndex == index) ?
            Positioned(
              left: SizeConfig.blockSizeHorizontal*4,
              bottom:  SizeConfig.blockSizeHorizontal*1.5,
              child: Icon(
              icon,
              color: Colors.black,
              size: SizeConfig.blockSizeHorizontal*8,
            ),):Container(),

            AnimatedOpacity(
              opacity: (currentIndex == index) ? 1 : 0.2,
              duration: Duration(milliseconds: 300),
              curve: Curves.easeIn,
              child: Icon(
                icon,
                color: Colors.yellow[300],
                size: SizeConfig.blockSizeHorizontal*8,
              ),
            ),
          ],
        ),
      ),
    );
  }
}